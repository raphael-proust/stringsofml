let ignore_comments = ref false

let strings file =
  let ic = open_in file in
  let lexbuf = Lexing.from_channel ic in
  let iterator =
    Ast_iterator.
      {
        default_iterator with
        expr =
          (fun iterator e ->
            match e.pexp_desc with
            | Pexp_constant (Pconst_string (s, _, _)) -> Format.printf "%S\n" s
            | _ -> default_iterator.expr iterator e);
      }
  in
  let iterator =
    if !ignore_comments then
      {
        iterator with
        attribute =
          (fun iterator a ->
            match a.attr_name.txt with
            | "ocaml.doc" | "ocaml.text" -> ()
            | _ -> Ast_iterator.default_iterator.attribute iterator a);
      }
    else iterator
  in
  let impl = Parse.implementation lexbuf in
  iterator.structure iterator impl;
  close_in ic

let _ =
  for i = 1 to Array.length Sys.argv - 1 do
    match Sys.argv.(i) with
    | "--ignore-comments" -> ignore_comments := true
    | file -> strings file
  done
