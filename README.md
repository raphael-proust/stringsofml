# stringsofml

A small tool to list all string literals of an ml file.

## Build

```
opam install ocaml-compiler-libs
dune build stringsofml.exe
```

## Use

```
_build/default/stringsofml.exe /some/path/to/an/ml/file.ml
```
